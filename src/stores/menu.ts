import { ref, createApp, reactive, computed } from "vue";
import { defineStore } from "pinia";
import type Coffee from "@/Types/Coffee";
import { useMessageStore } from "./message";
import ProductService from "@/services/product";
export const useCoffeeStore = defineStore("coffee", () => {
  const messageStore = useMessageStore();
  const coffees = ref<Coffee[]>([]);
  // {
  //   id: 1,
  //   type: "Drinks",
  //   name: "เอสเพรซโซ่",
  //   img: "../public/nobg/เอสเพรซโซ่.png",
  //   price: 30,
  // },
  // {
  //   id: 2,
  //   type: "Drinks",
  //   name: "คาปูชิโน่",
  //   img: "../public/nobg/คาปูชิโน่.png",
  //   price: 40,
  // },
  // {
  //   id: 3,
  //   type: "Drinks",
  //   name: "ลาเต้",
  //   img: "../public/nobg/ลาเต้.png",
  //   price: 40,
  // },
  // {
  //   id: 4,
  //   type: "Drinks",
  //   name: "ชามะนาว",
  //   img: "../public/nobg/ชามะนาว.png",
  //   price: 45,
  // },
  // {
  //   id: 5,
  //   type: "Drinks",
  //   name: "ชาเขียว",
  //   img: "../public/nobg/ชาเขียว.png",
  //   price: 50,
  // },
  // {
  //   id: 6,
  //   type: "Drinks",
  //   name: "นมสด",
  //   img: "../public/nobg/นมสด.png",
  //   price: 50,
  // },
  // {
  //   id: 7,
  //   type: "Drinks",
  //   name: "ชานม",
  //   img: "../public/nobg/ชานม.png",
  //   price: 50,
  // },
  // {
  //   id: 8,
  //   type: "Drinks",
  //   name: "สตอเบอรี่",
  //   img: "../public/nobg/สตอเบอรี่.png",
  //   price: 50,
  // },
  // {
  //   id: 9,
  //   type: "Drinks",
  //   name: "น้ำเปล่า",
  //   img: "../public/nobg/น้ำเปล่า.png",
  //   price: 50,
  // },
  // {
  //   id: 10,
  //   type: "Drinks",
  //   name: "น้ำส้ม",
  //   img: "../public/nobg/น้ำส้ม.png",
  //   price: 50,
  // },
  // {
  //   id: 11,
  //   type: "Dessert",
  //   name: "เค้กบานอฟฟี่",
  //   img: "../public/nobg/เค้กบานอฟฟี่.png",
  //   price: 20,
  // },
  // {
  //   id: 12,
  //   type: "Dessert",
  //   name: "ขนมไหว้พระจันทร์",
  //   img: "../public/nobg/ขนมไหว้พระจันทร์.png",
  //   price: 20,
  // },
  // {
  //   id: 13,
  //   type: "Dessert",
  //   name: "ค้อฟฟี่เค้ก",
  //   img: "../public/nobg/ค้อฟฟี่เค้ก.png",
  //   price: 20,
  // },
  // {
  //   id: 14,
  //   type: "Dessert",
  //   name: "คับเค้ก",
  //   img: "../public/nobg/คับเค้ก.png",
  //   price: 20,
  // },
  // {
  //   id: 15,
  //   type: "Dessert",
  //   name: "เค้กช็อกโกแล็ต",
  //   img: "../public/nobg/เค้กช็อกโกแล็ต.png",
  //   price: 20,
  // },
  // {
  //   id: 16,
  //   type: "Dessert",
  //   name: "เค้กเรดเวลเวด",
  //   img: "../public/nobg/เค้กเรดเวลเวด.png",
  //   price: 20,
  // },
  // {
  //   id: 17,
  //   type: "Dessert",
  //   name: "เค้กสตอรเบอรรี่",
  //   img: "../public/nobg/เค้กสตอรเบอรรี่.png",
  //   price: 20,
  // },
  // {
  //   id: 18,
  //   type: "Dessert",
  //   name: "คับเค้กช็อกโกแล็ตชิป",
  //   img: "../public/nobg/chocolate-cakes.png",
  //   price: 20,
  // },
  // {
  //   id: 19,
  //   type: "Dessert",
  //   name: "เค้กแครอท",
  //   img: "../public/nobg/delicious-slice-carrot-cake.png",
  //   price: 20,
  // },
  // {
  //   id: 20,
  //   type: "Dessert",
  //   name: "วอฟเฟอร์สตอรเบอรี่",
  //   img: "../public/nobg/waffles-with-strawberry.png",
  //   price: 20,
  // },
  // {
  //   id: 21,
  //   type: "MainDish",
  //   name: "กระเพราไข่ดาว",
  //   img: "../public/nobg/กระเพราไข่ดาว.png",
  //   price: 40,
  // },
  // {
  //   id: 22,
  //   type: "MainDish",
  //   name: "ไก่นึ่งมะนาว",
  //   img: "../public/nobg/ไก่นึ่งมะนาว.png",
  //   price: 40,
  // },
  // {
  //   id: 23,
  //   type: "MainDish",
  //   name: "ไก่ย่างบาร์บีคิว",
  //   img: "../public/nobg/ไก่ย่างบาร์บีคิว.png",
  //   price: 40,
  // },
  // {
  //   id: 24,
  //   type: "MainDish",
  //   name: "ข้าวขาหมู",
  //   img: "../public/nobg/ข้าวขาหมู.png",
  //   price: 40,
  // },
  // {
  //   id: 25,
  //   type: "MainDish",
  //   name: "ข้าวผัด",
  //   img: "../public/nobg/ข้าวผัด.png",
  //   price: 40,
  // },
  // {
  //   id: 26,
  //   type: "MainDish",
  //   name: "ข้าวผัดพริกแกง",
  //   img: "../public/nobg/ข้าวผัดพริกแกง.png",
  //   price: 40,
  // },
  // {
  //   id: 27,
  //   type: "MainDish",
  //   name: "ข้าวมันไก่",
  //   img: "../public/nobg/ข้าวมันไก่.png",
  //   price: 40,
  // },
  // {
  //   id: 28,
  //   type: "MainDish",
  //   name: "ข้าวหมูทอดกระเทียม",
  //   img: "../public/nobg/ข้าวหมูทอดกระเทียม.png",
  //   price: 40,
  // },
  // {
  //   id: 29,
  //   type: "MainDish",
  //   name: "แพนง",
  //   img: "../public/nobg/แพนง.png",
  //   price: 40,
  // },
  // {
  //   id: 30,
  //   type: "MainDish",
  //   name: "สเต็ก",
  //   img: "../public/nobg/สเต็ก.png",
  //   price: 40,
  // },

  const editCoffees = ref<Coffee[]>([
    { id: -1, type: null, name: "", img: "", price: 0 },
  ]);

  async function getProducts() {
    try {
      const res = await ProductService.getProducts();
      coffees.value = res.data;
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Product ได้");
    }
  }

  return {
    coffees,
    editCoffees,
    getProducts,
  };
});
