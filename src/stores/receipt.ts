import type { Receipt } from "@/Types/receipt";
import { defineStore } from "pinia";
import { ref } from "vue";

export const useReceiptStore = defineStore("receipt", () => {
  const rec = ref<Receipt[]>([]);
  const editReceipt = ref<Receipt>({
    id: -1,
    queue: -1,
    date: "",
    time: "",
    discount: 0,
    total: 0,
    received: 0,
    change: 0,
    payment: "",
  });

  const addRec = (id: number) => {};
  return { addRec, rec, editReceipt };
});
