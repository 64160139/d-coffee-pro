export default interface Coffee {
  id?: number;
  name: string;
  type?: string | null;
  img: string;
  price: number;
}
