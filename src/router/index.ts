import LoginView from "@/views/LoginView.vue";
import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import PosView from "../views/pos/PosView.vue";
const struser = localStorage.getItem("user");
const user = JSON.parse(struser!);
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      components: {
        default: HomeView,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/login",
      name: "login",
      components: {
        default: LoginView,
      },
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/pos",
      name: "pos",

      components: {
        default: () => import("../views/pos/PosView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
      },
    },
    {
      path: "/about",
      name: "about",

      components: {
        default: () => import("../views/AboutView.vue"),
        menu: () => import("@/components/menus/AboutMenu.vue"),
        header: () => import("@/components/headers/AboutHeader.vue"),
      },
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/stock",
      name: "stock",

      components: {
        default: () => import("@/views/StockView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/user",
      name: "user",

      components: {
        default: () => import("../views/UserView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/order",
      name: "order",

      components: {
        default: () => import("../views/OrderView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/customer",
      name: "customer",

      components: {
        default: () => import("../views/CustomerView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/employee",
      name: "employee",

      components: {
        default: () => import("../views/EmployeeView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/checkinout",
      name: "checkinout",

      components: {
        default: () => import("../views/CheckinoutView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/material",
      name: "material",

      components: {
        default: () => import("../views/MaterialView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/login",
      name: "login",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: LoginView,
    },
  ],
});

function isLogin() {
  if (user) {
    console.log(user.employee.position);
    return true;
  }
  return false;
}
router.beforeEach((to, from) => {
  if (to.meta.requiresAuth && !isLogin()) {
    return {
      path: "/login",
      query: { redirect: to.fullPath },
    };
  }
});

export default router;
